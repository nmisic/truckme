<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');

// php artisan route:list
Auth::routes();

Route::group(['middleware' => ['auth']], function () {

	/**
	 * Adds
	 */
	Route::get('/adds', 'AddsController@index')->name('adds.index');
	Route::post('/adds', 'AddsController@store')->name('adds.store');
  Route::get('/adds/search', 'AddsController@search')->name('adds.search');
	Route::get('/adds/create', 'AddsController@create')->name('adds.create');
  Route::get('/adds/my', 'AddsController@my')->name('adds.my');
  Route::get('/adds/type/{type}', 'AddsController@offersOrDemands')->name('adds.offersOrDemands');
	Route::get('/adds/{id}', 'AddsController@show')->name('adds.show');
  Route::get('/adds/category/{category}', 'AddsController@showCategory')->name('adds.category');
	Route::put('/adds/{id}', 'AddsController@update')->name('adds.update');
	Route::delete('/adds/{id}', 'AddsController@destroy')->name('adds.destroy');
	Route::get('/adds/{id}/edit', 'AddsController@edit')->name('adds.edit');
	Route::get('/adds/category/{categorySlug}', 'AddsController@category');
});
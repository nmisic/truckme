@extends('layouts.app')

@section('content')

    <div class="adds-top">
        <div class="columns">
            <div class="column is-three-quarters has-text-centered">

                <div class="level-item">
                    <div class="field has-addons">

                        <div class="level-left adds-number">
                            <div class="level-item">
                                <p class="subtitle">
                                    <a href="/adds/category/all">
                                        <strong>{{$addCount}}</strong> <span class="has-text-white">oglasa</span>
                                    </a>
                                </p>
                            </div>
                        </div>

                        <form action="/adds/search" method="GET" id="add-search">
                            <div class="field has-addons">
                                <div class="control">
                                    <input class="input" name="term" type="text" placeholder="Pretraži oglase ..." size="40">
                                </div>
                                <div class="control">
                                    <a class="button is-info" onclick="document.getElementById('add-search').submit();">
                                        Pretraži
                                    </a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
            <div class="column has-text-centered">
                <a href="{{  route('adds.create') }}" class="button is-info">
                    <span class="icon">
                        <i class="fa fa-plus-square"></i>
                    </span>
                    <span>Predaj oglas</span>
                </a>
            </div>
        </div>
    </div>

    <div class="truck-adds-grid has-text-centered">

        <div class="columns">
            <div class="column">
                <span class="adds-title">Generalni prijevoz</span>
                <div class="clearfix"></div>
                <a href="/adds/category/general">
                    <img src="/img/truck-icons/general-transport.png" alt="General transport truck">
                    <div class="clearfix"></div>
                    <span class="icon">
                        <i class="fa fa-newspaper-o">&nbsp;{{$addCategoryCount['generaltransport']}}</i>
                    </span>
                </a>
            </div>
        </div>

        <div class="columns">
            <div class="column">
                <span class="adds-title">Cisterne</span>
                <div class="clearfix"></div>
                <a href="/adds/category/cistern">
                    <img src="/img/truck-icons/cistern-transport.png" alt="Cistern transport truck">
                    <div class="clearfix"></div>
                    <span class="icon">
                        <i class="fa fa-newspaper-o">&nbsp;{{$addCategoryCount['cistern']}}</i>
                    </span>
                </a>
            </div>
            <div class="column">
                <span class="adds-title">Kiperi</span>
                <div class="clearfix"></div>
                <a href="/adds/category/tipper">
                    <img src="/img/truck-icons/tipper-transport.png" alt="Tipper transport truck">
                    <div class="clearfix"></div>
                    <span class="icon">
                        <i class="fa fa-newspaper-o">&nbsp;{{$addCategoryCount['tipper']}}</i>
                    </span>
                </a>
            </div>
            <div class="column">
                <span class="adds-title">Kontejneri</span>
                <div class="clearfix"></div>
                <a href="/adds/category/container">
                    <img src="/img/truck-icons/container-transport.png" alt="Container transport truck">
                    <div class="clearfix"></div>
                    <span class="icon">
                        <i class="fa fa-newspaper-o">&nbsp;{{$addCategoryCount['container']}}</i>
                    </span>
                </a>
            </div>
        </div>

        <div class="columns">
            <div class="column">
                <span class="adds-title">Hladnjače</span>
                <div class="clearfix"></div>
                <a href="/adds/category/refrigerated">
                    <img src="/img/truck-icons/refrigerated-transport.png" alt="Refrigerated transport truck">
                    <div class="clearfix"></div>
                    <span class="icon">
                        <i class="fa fa-newspaper-o">&nbsp;{{$addCategoryCount['refrigerated']}}</i>
                    </span>
                </a>
            </div>

            <div class="column">
                <span class="adds-title">Opasne tvari</span>
                <div class="clearfix"></div>
                <a href="/adds/category/hazard">
                    <img src="/img/truck-icons/hazard-transport.png" alt="Hazard transport truck">
                    <div class="clearfix"></div>
                    <span class="icon">
                        <i class="fa fa-newspaper-o">&nbsp;{{$addCategoryCount['hazard']}}</i>
                    </span>
                </a>
            </div>

            <div class="column">
                <span class="adds-title">Poseban prijevoz</span>
                <div class="clearfix"></div>
                <a href="/adds/category/special">
                    <img src="/img/truck-icons/special-transport.png" alt="Special transport truck">
                    <div class="clearfix"></div>
                    <span class="icon">
                        <i class="fa fa-newspaper-o">&nbsp;{{$addCategoryCount['special']}}</i>
                    </span>
                </a>
            </div>
        </div>

        <div class="columns">
            <div class="column">
                <span class="adds-title">Dizalice i kranovi</span>
                <div class="clearfix"></div>
                <a href="/adds/category/crane">
                    <img src="/img/truck-icons/crane-transport.png" alt="Crane transport truck">
                    <div class="clearfix"></div>
                    <span class="icon">
                        <i class="fa fa-newspaper-o">&nbsp;{{$addCategoryCount['crane']}}</i>
                    </span>
                </a>
            </div>

            <div class="column">
                <span class="adds-title">Vučna služba</span>
                <div class="clearfix"></div>
                <a href="/adds/category/towing-service">
                    <img src="/img/truck-icons/towing-service-transport.png" alt="Towing service transport truck">
                    <div class="clearfix"></div>
                    <span class="icon">
                        <i class="fa fa-newspaper-o">&nbsp;{{$addCategoryCount['towing']}}</i>
                    </span>
                </a>
            </div>

            <div class="column">
                <span class="adds-title">Prijevoz plovila</span>
                <div class="clearfix"></div>
                <a href="/adds/category/vessel">
                    <img src="/img/truck-icons/vessel-transport.png" alt="Vessel transport truck">
                    <div class="clearfix"></div>
                    <span class="icon">
                        <i class="fa fa-newspaper-o">&nbsp;{{$addCategoryCount['vessel']}}</i>
                    </span>
                </a>
            </div>
        </div>

    </div>
@endsection

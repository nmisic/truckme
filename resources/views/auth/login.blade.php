@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="columns is-vcentered">
            <div class="column is-4 is-offset-4">
                <h1 class="title">
                    Prijavi se
                </h1>
                <div class="box">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <label class="label">Email</label>
                        <p class="control">
                            <input id="email" type="email" class="input" name="email" value="{{ old('email') }}"
                                   required autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </p>

                        <label class="label">Lozinka</label>
                        <p class="control">
                            <input id="password" class="input" type="password" class="form-control" name="password"
                                   required>

                            @if ($errors->has('password'))
                                <span class="help-block text-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </p>
                        <p class="control" style="margin: 15px 0;">
                            <button class="button is-primary">Prijava</button>
                        </p>
                        <label>
                            <input type="checkbox" class="checkbox"
                                   name="remember" {{ old('remember') ? 'checked' : '' }}> Zapamti me
                        </label>
                    </form>
                </div>
                {{--
                <p class="has-text-centered">
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        Zaboravili ste lozinku?
                    </a>
                </p>
                --}}
            </div>
        </div>
    </div>

@endsection

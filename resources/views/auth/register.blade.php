@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="columns is-vcentered">
            <div class="column is-4 is-offset-4">
                <h1 class="title">
                    Registriraj se
                </h1>
                <div class="box">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <label class="label">Ime</label>
                        <p class="control">
                            <input id="name" type="text" class="input" name="name" value="{{ old('name') }}"
                                   required autofocus>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </p>

                        <label class="label">Email</label>
                        <p class="control">
                            <input id="email" type="email" class="input" name="email" value="{{ old('email') }}"
                                   required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </p>

                        <label class="label">Lozinka</label>
                        <p class="control">
                            <input id="password" type="password" class="input" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </p>

                        <label class="label">Potvrdite lozinku</label>
                        <p class="control">
                            <input id="password-confirm" type="password" class="input"
                                   name="password_confirmation" required>
                        </p>
                        </p>
                        <p class="control" style="margin: 15px 0;">
                            <button class="button is-primary">Registracija</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

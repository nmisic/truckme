@extends('layouts.app')

@section('content')

    <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
            <li><a href="/home">Naslovnica</a></li>
            <li class="is-active"><a href="#">{{$categoryName}}</a></li>
        </ul>
    </nav>

    <div class="columns">
        <div class="column is-4">
            <h2 class="title is-2">{{$categoryName}}</h2>
        </div>
        <div class="column is-2 is-offset-6">
            <div class="column has-text-centered">
                <a href="{{  route('adds.create') }}" class="button is-info">
                    <span class="icon">
                        <i class="fa fa-plus-square"></i>
                    </span>
                    <span>Predaj oglas</span>
                </a>
            </div>
        </div>
    </div>

    @if($categorySlug AND ($categorySlug == 'search'))
        <div class="columns">
            <div class="column">
                <form action="/adds/search" method="GET" id="add-search">
                    <div class="field has-addons">
                        <div class="control">
                            <input class="input" name="term" type="text" placeholder="Pretraži oglase ..." size="40">
                        </div>
                        <div class="control">
                            <a class="button is-info" onclick="document.getElementById('add-search').submit();">
                                Pretraži
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endif

    @if (session('status'))
        <div class="block" id="status">
            <span class="tag is-primary">
                {{ session('status') }}
                <button class="delete is-small"></button>
         </span>
        </div>
    @endif

    @if(count($adds) == 0)
        <article class="message is-primary">
            <div class="message-header">
                <p>Ooops!</p>
            </div>
            <div class="message-body">
                Kategorija trenutno nema aktivnih oglasa. <a style="link" href="{{route('home')}}">Naslovnica ...</a>
            </div>
        </article>
    @else
        <div class="columns">
            <div class="column">

                @foreach($adds as $add)

                    <div class="box">
                        <div class="columns">
                            <div class="column is-3">
                                @if($add->type == 'demand')
                                    <span class="has-text-primary">Tražim :: </span>
                                @else
                                    <span class="has-text-primary">Nudim :: </span>
                                @endif
                                <strong>
                                    @if($add->start_city)
                                        {{$add->start_city}}
                                    @else
                                        bilo koje
                                    @endif
                                </strong>
                                <span class="icon is-small"><i class="fa fa-arrow-right"></i></span>
                                <strong>
                                    @if($add->end_city)
                                        {{$add->end_city}}
                                    @else
                                        bilo koje
                                    @endif
                                </strong>
                            </div>
                            <div class="column is-2 has-text-centered">
                                <span class="has-text-info">{{$add->users_name}}</span>
                            </div>
                            <div class="column is-2">
                                <span class="icon is-small"><i class="fa fa-phone"></i></span> {{$add->contact}}
                            </div>
                            <div class="column is-4">
                                <span class="icon is-small"><i class="fa fa-calendar"></i></span>
                                @if($add->date AND $add->date_to)

                                    {{Carbon\Carbon::parse($add->date)->format('d.m.Y')}}
                                    do {{Carbon\Carbon::parse($add->date_to)->format('d.m.Y')}}

                                @elseif($add->date AND is_null($add->date_to))

                                    {{Carbon\Carbon::parse($add->date)->format('d.m.Y')}}

                                @elseif(is_null($add->date) AND $add->date_to)

                                    do {{Carbon\Carbon::parse($add->date_to)->format('d.m.Y')}}

                                @elseif(is_null($add->date) AND is_null($add->date_to))
                                    bilo koji
                                @endif
                            </div>
                            <div class="column">
                                <a href="/adds/{{$add->id}}" title="Pregledaj oglas"><span class="icon"><i
                                                class="fa fa-newspaper-o"></i></span></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif

@endsection

@section('script')
    <script>
      setTimeout(function () {
        $("#status").remove();
      }, 5000);
    </script>
@endsection
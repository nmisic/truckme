@extends('layouts.app')

@section('content')

    <form method="POST" action="{{ route('adds.store')  }}">
        {{csrf_field()}}
        <div class="box" style="background-color: whitesmoke;">

            <div class="columns">
                <div class="column">
                    <h2 class="title is-2">Predaj oglas</h2>
                    <hr class="is-primary">
                </div>
            </div>

            @if ($errors->any())
                <div class="columns">
                    <div class="column">
                        <article class="message is-info">
                            <div class="message-header">
                                <p>Ooooops!</p>
                            </div>
                            <div class="message-body">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        @if ($errors->has('add_type'))
                                            <li> - {{ $error }}</li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </article>
                    </div>
                </div>
            @endif

            <div class="columns">
                <div class="column">
                    <div class="field is-horizontal">
                        <div class="field-label">
                            <label class="label">Tip oglasa<span class="has-text-danger">*</span> </label>
                        </div>
                        <div class="field-body">
                            <div class="field is-narrow">
                                <div class="control">
                                    <label class="radio">
                                        <input type="radio" name="add_type" value="demand">
                                        Tražim prijevoz
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="add_type" value="offer">
                                        Nudim prijevoz
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- END TIP OGLASA -->

            <div class="columns">
                <div class="column">
                    <div class="field is-horizontal">
                        <div class="field-label">
                            <label class="label">Kategorija<span class="has-text-danger">*</span> </label>
                        </div>
                        <div class="field-body">
                            <div class="field is-narrow">
                                <div class="control">
                                    <div class="select">
                                        <select class="adds-select-box" name="categoryid">
                                            @foreach($categories as $category)
                                                @if ($loop->first)
                                                    <option value="{{$category->id}}"
                                                            select>{{$category->truck_category}}</option>
                                                @else
                                                    <option value="{{$category->id}}">{{$category->truck_category}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- END TIP KATEGORIJA PRIJEVOZA -->

            <div class="columns">
                <div class="column">
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">
                            <span class="icon is-small">
                                <i class="fa fa-map-marker"></i>
                            </span> Polazište
                            </label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="select">
                                    <select class="adds-select-box" name="startpoint">
                                        <option value="">Bilo koje</option>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}">{{$city->city_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="field-label is-normal">
                                <label class="label">
                            <span class="icon is-small">
                                <i class="fa fa-map-marker"></i>
                            </span> Odredište
                                </label>
                            </div>
                            <div class="field">
                                <div class="select">
                                    <select class="adds-select-box" name="endpoint">
                                        <option value="">Bilo koje</option>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}">{{$city->city_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- END POLAZISTE ODREDISTE -->

            <div class="columns">
                <div class="column">
                    <div class="field is-horizontal">
                        <div class="field-label">
                            <label class="label">
                                <span class="icon is-small"><i class="fa fa-balance-scale"></i></span> Težina
                            </label>
                        </div>
                        <div class="field-body">
                            <div class="field is-narrow">
                                <div class="control">
                                    <input type="radio" name="weight" value="0"  checked="checked" hidden>
                                    @foreach($weights as $weight)
                                        <label class="radio">
                                            <input type="radio" name="weight" value="{{ $weight->id  }}">
                                            {{ $weight->value  }} kg
                                        </label>
                                    @endforeach
                                </div>
                                <p class="help">
                                    <span class="icon is-small"><i class="fa fa-info-circle"></i></span>
                                    Odaberite maksimalnu težinu transporta
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- END TEZINA TRANSPORTA -->

            <div class="columns">
                <div class="column">
                    <div class="field is-horizontal">
                        <div class="field-label">
                            <label class="label">
                            <span class="icon is-small">
                                <i class="fa fa-arrows-alt"></i>
                            </span> Dimenzije
                            </label>
                        </div>
                        <div class="field-body">
                            <div class="field is-fullwidth">
                                <div class="control">
                                    <input class="input" type="text" name="dimensions"
                                           placeholder="Unesite dimenzije transporta ...">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- END DIMENZIJE -->

            <div class="columns">
                <div class="column">
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">
                            <span class="icon is-small">
                                <i class="fa fa-calendar"></i>
                            </span> Datum
                            </label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <input class="input add-date-input" name="date" type="text" id="date" placeholder="dd.mm.yyyy">
                            </div>
                            <div class="field-label is-normal">
                                <label class="label">
                            <span class="icon is-small">
                                <i class="fa fa-calendar"></i>
                            </span> Datum do
                                </label>
                            </div>
                            <div class="field">
                                <input class="input add-date-input" name="date-to" type="text" id="date-to" placeholder="dd.mm.yyyy">
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- END DATUM OD DO -->

            <div class="columns">
                <div class="column">
                    <div class="field is-horizontal">
                        <div class="field-label">
                            <label class="label">
                            <span class="icon is-small">
                                <i class="fa fa-comment"></i>
                            </span> Opis
                            </label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                <textarea class="textarea" name="description" id="" cols="30" rows="10"
                                          placeholder="Unesite dodatne informacije ..."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- END OPIS -->

            <div class="columns">
                <div class="column">
                    <div class="field is-horizontal">
                        <div class="field-label">
                            <label class="label">
                            <span class="icon is-small">
                                <i class="fa fa-phone"></i>
                            </span> Kontakt<span class="has-text-danger">*</span>
                            </label>
                        </div>
                        <div class="field-body">
                            <div class="field is-fullwidth">
                                <div class="control">
                                    <input class="input" type="text" name="contact" placeholder="Unesite kontak broj ..." required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- END KONTAKT -->

            <div class="columns">
                <div class="column">
                    <div class="field is-horizontal">
                        <div class="field-label">
                            <label class="label">
                                &nbsp;
                            </label>
                        </div>
                        <div class="field-body">
                            <div class="field is-fullwidth">
                                <div class="control">
                                    <div class="field is-grouped">
                                        <div class="control">
                                            <button type="submit" class="button is-primary">Predaj oglas</button>
                                        </div>
                                        <div class="control">
                                            <button class="button is-link">Odustani</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- END SUBMIT -->
    </form>

    </div> <!-- END BOX -->
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $(function () {
                $("#date").datepicker({
                    firstDay: 1,
                    dateFormat: 'dd.mm.yy'
                });
            });

            $(function () {
                $("#date-to").datepicker({
                    firstDay: 1,
                    dateFormat: 'dd.mm.yy'
                });
            });
        });
    </script>
@endsection
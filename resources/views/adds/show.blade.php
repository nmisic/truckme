@extends('layouts.app')

@section('content')

    <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
            <li><a href="/home">Naslovnica</a></li>
            <li><a href="/adds/category/{{$add->truck_category_slug}}">{{$add->truck_category}}</a></li>
            <li class="is-active"><a href="#">Detalji oglasa</a></li>
        </ul>
    </nav>

    <div class="box" style="background-color: whitesmoke;">

        <div class="columns">
            <div class="column">
                <div class="adds-destination">
                    @if($add->type == 'demand')
                        <h2 class="title is-2">Tražim prijevoz :: {{$add->truck_category}}</h2>
                    @else
                        <h2 class="title is-2">Nudim prijevoz :: {{$add->truck_category}}</h2>
                    @endif
                </div>
                <hr class="is-primary">
            </div>
        </div>

        <div class="columns">
            <div class="column is-one-quarter">
                <div class="media-content">

                    <article class="media">
                        <figure class="media-left">
                            <p class="image">
                                <!-- TODO if statement to display correct icon depending on transport type -->
                                <img src="/img/truck-icons/{{$add->truck_category_slug}}-transport.png"
                                     alt="General transport truck">
                            </p>
                        </figure>
                    </article>
                </div>


            </div>

            <div class="column">
                <div class="columns">
                    <div class="column">
                        <span class="title is-3 has-text-primary">
                            @if($add->start_city)
                                {{$add->start_city}}
                            @else
                                bilo koje
                            @endif
                        </span>
                        <span class="icon is-medium">
                                <i class="fa fa-arrow-right"></i>
                            </span>
                        <span class="has-text-info title is-3">
                            @if($add->end_city)
                                {{$add->end_city}}
                            @else
                                bilo koje
                            @endif
                        </span>
                    </div>
                </div>


                <div class="columns">
                    <div class="column">
                        <span class="icon is-small"><i class="fa fa-user"></i></span>
                        <strong>Ime: </strong>
                        <span>{{$add->users_name}}</span>
                    </div>
                </div>
                <div class="columns">
                    <div class="column">
                        <span class="icon is-small"><i class="fa fa-phone"></i></span>
                        <strong>Kontakt: </strong>
                        <span>
                            @if($add->contact)
                                {{$add->contact}}
                            @else
                                N/A
                            @endif
                        </span>
                    </div>
                </div>
                <div class="columns">
                    <div class="column">
                        <span class="icon is-small"><i class="fa fa-balance-scale"></i></span>
                        <strong>Težina: </strong>
                        <span>
                            @if($add->weight)
                                {{$add->weight}} kg
                            @else
                                N/A
                            @endif
                        </span>
                    </div>
                </div>
                <div class="columns">
                    <div class="column">
                        <span class="icon is-small"><i class="fa fa-arrows-alt"></i></span>
                        <strong>Dimenzije: </strong>
                        <span>
                            @if($add->dimensions)
                                {{$add->dimensions}}
                            @else
                                N/A
                            @endif
                        </span>
                    </div>
                </div>
                <div class="columns">
                    <div class="column">
                        <span class="icon is-small"><i class="fa fa-calendar"></i></span>
                        <strong>Datum: </strong>
                        <span>
                            @if($add->date AND $add->date_to)

                                {{Carbon\Carbon::parse($add->date)->format('d.m.Y')}}
                                do {{Carbon\Carbon::parse($add->date_to)->format('d.m.Y')}}

                            @elseif($add->date AND is_null($add->date_to))

                                {{Carbon\Carbon::parse($add->date)->format('d.m.Y')}}

                            @elseif(is_null($add->date) AND $add->date_to)

                                do {{Carbon\Carbon::parse($add->date_to)->format('d.m.Y')}}

                            @elseif(is_null($add->date) AND is_null($add->date_to))
                                bilo koji
                            @endif
                        </span>
                    </div>
                </div>
                <div class="columns">
                    <div class="column">
                        <span class="icon is-small"><i class="fa fa-comment"></i></span>
                        <strong>Opis</strong>
                        <br>
                        @php
                            $description = $add->description ? $add->description : 'N/A';
                        @endphp
                        <span>{{$description}}</span>
                    </div>
                </div>
            </div>

            <div class="column">
                <div style="margin-bottom: 10px;">
                    <strong>Oglas kreiran: </strong>
                    <span>{{Carbon\Carbon::parse($add->created_at)->format('d.m.Y')}}</span>
                </div>

                @if($add->user_id == Auth::id())
                    <form action="/adds/{{$add->id}}" method="POST" id="delete-add">
                        {{ csrf_field() }}
                        <input name="_method" type="hidden" value="DELETE"/>
                        <a class="button is-medium is-danger" onclick="document.getElementById('delete-add').submit();">
                            <span class="icon"><i class="fa fa-trash-o"></i></span>
                            <span>Deaktiviraj oglas</span>
                        </a>
                    </form>
                @endif
            </div>

        </div>
    </div><!-- END BOX -->

@endsection

@section('script')

@endsection
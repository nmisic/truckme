<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="icon" href="/img/favicon-32x32.png" type="image/x-icon"/>

    <!-- Styles -->
    <link href="{{ asset('css/bulma.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">

    <nav class="navbar navbar-app">
        <div class="navbar-brand">
            <a class="navbar-item navbar-brand-title" href="{{ url('/') }}">
                <img src="/img/brand-logo.png" alt="TruckMee logo">
                {{ config('app.name', 'TruckMee') }}
            </a>
        </div>

        <div id="navMenuExample" class="navbar-menu">
            <div class="navbar-start">
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link " href="/adds/category/all">
                        Oglasi
                    </a>
                    <div class="navbar-dropdown ">
                        <a class="navbar-item" href="/adds/category/all">
                            Svi oglasi
                        </a>
                        <a class="navbar-item" href="/adds/type/offer">
                            Ponuda
                        </a>
                        <a class="navbar-item" href="/adds/type/demand">
                            Potražnja
                        </a>
                        <hr class="navbar-divider">
                        <a class="navbar-item" href="/adds/my">
                            Moji oglasi
                        </a>
                        <a class="navbar-item" href="/adds/create">
                            Predaj oglas
                        </a>
                    </div>
                </div>
                {{--
                <a class="navbar-item" href="/truckers">
                    Prijevoznici
                </a>
                --}}
            </div>

            <div class="navbar-end">
                <!-- Authentication Links -->

                @if (Auth::guest())
                    <div class="navbar-item">
                        <a href="{{ route('login') }}" class="navbar-item">Prijava</a>
                    </div>
                    <div class="navbar-item">
                        <a href="{{ route('register') }}" class="navbar-item">Registriraj se</a>
                    </div>
                @else
                    <div class="navbar-item has-dropdown is-hoverable">
                        <a href="#" class="navbar-link">
                            <span class="icon"><i class="fa fa-user"></i></span> {{ Auth::user()->name }}
                        </a>
                        <div class="navbar-dropdown ">
                            <a class="navbar-item " href="/logout" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Odjava
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </nav>

    <div class="container app-container is-fluid">
        @yield('content')
    </div>

    <div class="footer">
        <div class="tabs is-centered">
            <ul>
                <li><a href="https://twitter.com/n_misic" target="_blank">&copy; Nikola Mišić</a></li>
            </ul>
        </div>
    </div>

</div>

<!-- Scripts -->
<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.js') }}"></script>
@yield('script')
</body>
</html>

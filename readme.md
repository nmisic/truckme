# TruckMee
Hey there! TruckMee is a truckin' app. Its goal is to enable transport adds for truckers and clients.

## Installation

TruckMee is built with Laravel PHP framework. Make sure you have Laravel 5.3  [requirements](https://laravel.com/docs/5.3/installation#server-requirements) covered on your system.

Place your vhost path to the "public" folder in the root of the application. Take a look at the official [documentation](https://laravel.com/docs/5.3/installation#configuration) for more information.

Install composer dependencies. In the project root run the command:
```sh
$ composer install
```
Copy ".env.example" file and name it ".env". After that, open a new database and set DB variables in the ".env" file accordingly.

OK, let's build up the database schema.
```sh
$ php artisan migrate
```

We need some data to start with, run the seeders.
```sh
$ php artisan db:seed
```
That's it! You can now register a new account or use the default.

Username: nmisic@nmisic.com
Password: nmisic

Yes, this is terrible, but nice for a demo if you don't want to register a new account.

## The Code
Laravel, like any other framework, provides a lot of the code out-of-the-box. Application specific logic is placed inside these components.
### Routes

You can find the app routes in "routes/web.php" file. List all routes using artisan command:
```sh
$ php artisan route:list
```

### Controllers
Look into the "app/Http/Controllers" folder. Controllers are structured in a REST architecture.

### Models
Models can be found in the "app" folder.
### Views
Views are placed in "resources/views" folder. They are also sorted by the given resource and share a name with the associated method in the controller.
### Requests
See "app/Http/Requests". Laravel provides a nice way to validate more complex form requests. Here you can find the add submission validation script. Other validations, such as simple id in GET request, are placed in controllers.
### Database
In the path "database" you can find "migrations" and "seeds" folders. Here is the logic behind database schema and seed data.

### JavaScript and CSS
JavaScript, CSS, and images can be found in the "public" folder.

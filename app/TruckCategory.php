<?php

namespace TruckMee;

use Illuminate\Database\Eloquent\Model;

class TruckCategory extends Model
{
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;

}

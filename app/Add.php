<?php

namespace TruckMee;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Add extends Model {

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    //
  ];

  /**
   * Stores new Add object.
   *
   * @param $data
   *
   * @return Add
   */
  public function store($data) {
    $add = new Add();
    $add->type = $data['add_type'];
    $add->user_id = Auth::id();
    $add->description = $data['description'];
    $add->contact = $data['contact'];
    $add->start_point = $data['startpoint'];
    $add->end_point = $data['endpoint'];
    $add->category_id = $data['categoryid'];
    $add->weight = $data['weight'];
    $add->dimensions = $data['dimensions'];
    $add->date = $data['date'] ? date('Y-m-d 00:00:00', strtotime($data['date'])) : NULL;
    $add->date_to = $data['date-to'] ? date('Y-m-d 00:00:00', strtotime($data['date-to'])) : NULL;
    $add->save();

    return $add;
  }

  /**
   * Returns adds from currently logged in user.
   *
   * @return mixed
   */
  public function getMyAdds()
  {
    $adds =  DB::table('adds')
      ->leftJoin('cities AS cities_start', 'adds.start_point', '=', 'cities_start.id')
      ->leftJoin('cities AS cities_end', 'adds.end_point', '=', 'cities_end.id')
      ->leftJoin('users', 'adds.user_id', '=', 'users.id')
      ->leftJoin('weights', 'adds.weight', '=', 'weights.id')
      ->leftJoin('truck_categories', 'adds.category_id', '=', 'truck_categories.id')
      ->where('adds.deleted_at', '=', NULL)
      ->where('adds.user_id', '=', Auth::id())
      ->select(
        'adds.*',
        'cities_start.city_name as start_city',
        'cities_end.city_name as end_city',
        'users.name as users_name',
        'weights.value as weight'
      )
      ->get();

    return $adds;
  }

  /**
   * Returns list of add offers or demands, depending
   * on $type parametar.
   *
   * @param $type
   *
   * @return mixed
   */
  public function getOffersOrDemands($type)
  {
    $adds =  DB::table('adds')
      ->leftJoin('cities AS cities_start', 'adds.start_point', '=', 'cities_start.id')
      ->leftJoin('cities AS cities_end', 'adds.end_point', '=', 'cities_end.id')
      ->leftJoin('users', 'adds.user_id', '=', 'users.id')
      ->leftJoin('weights', 'adds.weight', '=', 'weights.id')
      ->leftJoin('truck_categories', 'adds.category_id', '=', 'truck_categories.id')
      ->where('adds.deleted_at', '=', NULL)
      ->where('adds.type', '=', $type)
      ->select(
        'adds.*',
        'cities_start.city_name as start_city',
        'cities_end.city_name as end_city',
        'users.name as users_name',
        'weights.value as weight'
      )
      ->get();

    return $adds;
  }

  /**
   * Returns all active adds.
   *
   * @return mixed
   */
  public function getAllAdds()
  {
    $adds =  DB::table('adds')
      ->leftJoin('cities AS cities_start', 'adds.start_point', '=', 'cities_start.id')
      ->leftJoin('cities AS cities_end', 'adds.end_point', '=', 'cities_end.id')
      ->leftJoin('users', 'adds.user_id', '=', 'users.id')
      ->leftJoin('weights', 'adds.weight', '=', 'weights.id')
      ->leftJoin('truck_categories', 'adds.category_id', '=', 'truck_categories.id')
      ->where('adds.deleted_at', '=', NULL)
      ->select(
        'adds.*',
        'cities_start.city_name as start_city',
        'cities_end.city_name as end_city',
        'users.name as users_name',
        'weights.value as weight'
      )
      ->get();

    return $adds;
  }

  /**
   * Returns all details about specific add.
   *
   * @param $id
   *
   * @return mixed
   */
  public function show($id)
  {
    $add =  DB::table('adds')
      ->leftJoin('cities AS cities_start', 'adds.start_point', '=', 'cities_start.id')
      ->leftJoin('cities AS cities_end', 'adds.end_point', '=', 'cities_end.id')
      ->leftJoin('users', 'adds.user_id', '=', 'users.id')
      ->leftJoin('weights', 'adds.weight', '=', 'weights.id')
      ->leftJoin('truck_categories', 'adds.category_id', '=', 'truck_categories.id')
      ->where('adds.id', '=', $id)
      ->where('adds.deleted_at', '=', NULL)
      ->select(
        'adds.*',
        'cities_start.city_name as start_city',
        'cities_end.city_name as end_city',
        'users.name as users_name',
        'weights.value as weight',
        'truck_categories.truck_category as truck_category',
        'truck_categories.truck_category_slug as truck_category_slug'
      )
      ->first();

    return $add;
  }

  public function showCategory($categorySlug)
  {
    if ($categorySlug == 'all') {
      return $this->getAllAdds();
    }

    $adds =  DB::table('adds')
      ->leftJoin('cities AS cities_start', 'adds.start_point', '=', 'cities_start.id')
      ->leftJoin('cities AS cities_end', 'adds.end_point', '=', 'cities_end.id')
      ->leftJoin('users', 'adds.user_id', '=', 'users.id')
      ->leftJoin('weights', 'adds.weight', '=', 'weights.id')
      ->leftJoin('truck_categories', 'adds.category_id', '=', 'truck_categories.id')
      ->where('truck_categories.truck_category_slug', '=', $categorySlug)
      ->select(
        'adds.*',
        'cities_start.city_name as start_city',
        'cities_end.city_name as end_city',
        'users.name as users_name',
        'weights.value as weight'
      )
      ->get();

    return $adds;
  }

  /**
   * Returns number of adds.
   *
   * @return int
   */
  public function getCount()
  {
    return Add::count();
  }

  /**
   * Returns count of adds for each truck category.
   *
   * @return mixed
   */
  public function getCategoryCount()
  {
    $category['generaltransport'] = DB::table('adds')->where('category_id', '=', 2)->count();
    $category['cistern'] = DB::table('adds')->where('category_id', '=', 3)->count();
    $category['tipper'] = DB::table('adds')->where('category_id', '=', 4)->count();
    $category['container'] = DB::table('adds')->where('category_id', '=', 5)->count();
    $category['refrigerated'] = DB::table('adds')->where('category_id', '=', 6)->count();
    $category['hazard'] = DB::table('adds')->where('category_id', '=', 7)->count();
    $category['special'] = DB::table('adds')->where('category_id', '=', 8)->count();
    $category['crane'] = DB::table('adds')->where('category_id', '=', 9)->count();
    $category['towing'] = DB::table('adds')->where('category_id', '=', 10)->count();
    $category['vessel'] = DB::table('adds')->where('category_id', '=', 11)->count();

    return $category;

  }

  /**
   * Returns category name for given category slug.
   *
   * @param $categorySlug
   *
   * @return mixed
   */
  public function getCategoryName($categorySlug)
  {
   return DB::table('truck_categories')
     ->where('truck_category_slug', '=', $categorySlug)
     ->first()
     ->truck_category;
  }

  public function search($term)
  {
    $term = '%' . $term . '%';

    $adds =  DB::table('adds')
      ->leftJoin('cities AS cities_start', 'adds.start_point', '=', 'cities_start.id')
      ->leftJoin('cities AS cities_end', 'adds.end_point', '=', 'cities_end.id')
      ->leftJoin('users', 'adds.user_id', '=', 'users.id')
      ->leftJoin('weights', 'adds.weight', '=', 'weights.id')
      ->leftJoin('truck_categories', 'adds.category_id', '=', 'truck_categories.id')
      ->where('cities_start.city_name', 'LIKE', $term)
      ->orWhere('cities_end.city_name', 'LIKE', $term)
      ->orWhere('adds.description', 'LIKE', $term)
      ->select(
        'adds.*',
        'cities_start.city_name as start_city',
        'cities_end.city_name as end_city',
        'users.name as users_name',
        'weights.value as weight'
      )
      ->get();

    return $adds;
  }

}

<?php

namespace TruckMee\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddsStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'add_type' => 'required|in:offer,demand|max:20',
			'startpoint' => 'numeric|nullable',
			'endpoint' => 'numeric|nullable',
			'weight' => 'string|nullable|digits_between:0,6|max:255', // for digits_between see weights table
			'dimensions' => 'string|nullable|max:255',
			'description' => 'max:255',
			'contact' => 'required|max:255'
        ];
    }

    public function messages()
	{
		return [
			'add_type.required' => 'Tip oglasa je obavezan odabir',
      'contact.required' => 'Potrebno je upisati kontakt'
		];
	}
}

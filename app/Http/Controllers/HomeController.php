<?php

namespace TruckMee\Http\Controllers;

use Illuminate\Http\Request;
use TruckMee\Add;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $add = new Add();
      $addCount = $add->getCount();
      $addCategoryCount = $add->getCategoryCount();

      return view('home')
          ->with('addCount', $addCount)
          ->with('addCategoryCount', $addCategoryCount);
    }
}

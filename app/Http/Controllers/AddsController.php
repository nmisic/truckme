<?php

namespace TruckMee\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use TruckMee\Http\Requests\AddsStoreRequest;
use TruckMee\City;
use TruckMee\Weight;
use TruckMee\Add;
use TruckMee\TruckCategory;

class AddsController extends Controller
{

	private $add;

  /**
   * AddsController constructor.
   */
	public function __construct()
	{
		$this->add = new Add;
	}

	/**
	 * Display a listing of the adds.
	 * GET /adds
	 */
	public function index()
	{
    $add = new Add();
    $addCount = $add->getCount();
    $addCategoryCount = $add->getCategoryCount();

    return view('home')
      ->with('addCount', $addCount)
      ->with('addCategoryCount', $addCategoryCount);
	}

	/**
	 * Show the form for creating a new add.
	 * GET /adds/create
	 */
	public function create()
	{
		return view('adds.create')
			->with('cities', City::all())
			->with('weights', Weight::all())
			->with('categories', TruckCategory::all()->except(1)); # id 1 is reserved for "all"
	}

	/**
	 * Store a newly created add.
	 * POST /adds
	 */
	public function store(AddsStoreRequest $request)
	{
		$add = $this->add->store($request->except('_token'));
		return redirect()
			->route('adds.show', ['id' => $add->id]);
	}

	/**
	 * Show the details for the specified resource.
	 * GET /adds/{id}
	 *
	 * @param $id
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
	public function show($id)
	{
		$validation = Validator::make(
			['id' => $id],
			['id' => 'required|exists:adds,id,deleted_at,NULL']
		);

		if($validation->fails()) {
			return redirect()->back();
		}

		$add = $this->add->show($id);

		return view('adds.show')
			->with('add', $add);
	}

  /**
   * Return list of adds for logged in user.
   *
   * @return $this
   */
	public function my()
  {
    $adds = $this->add->getMyAdds();
    $categoryName = "Moji oglasi";

    return view('adds.showCategory')
      ->with('adds', $adds)
      ->with('categoryName', $categoryName)
      ->with('categorySlug', NULL);
  }

  public function offersOrDemands($type)
  {
    $validation = Validator::make(
      ['type' => $type],
      ['type' => 'required|string|in:offer,demand']
    );

    if($validation->fails()) {
      return redirect()->back();
    }

    $adds = $this->add->getOffersOrDemands($type);

    if($type == 'offer') {
      $categoryName = "Ponuda prijevoza";
    } else if($type == "demand") {
      $categoryName = "Potražnja prijevoza";
    }

    return view('adds.showCategory')
      ->with('adds', $adds)
      ->with('categoryName', $categoryName)
      ->with('categorySlug', NULL);
  }

  /**
   * Displays all adds by given category.
   * GET /adds/category/{category}
   *
   * @param $category
   *
   * @return \Illuminate\Http\RedirectResponse
   */
	public function showCategory($category)
  {
    $validation = Validator::make(
      ['category' => $category],
      ['category' => 'required|string|exists:truck_categories,truck_category_slug']
    );

    if($validation->fails()) {
      return redirect()->back();
    }

    $adds = $this->add->showCategory($category);
    $categoryName = $this->add->getCategoryName($category);

    return view('adds.showCategory')
      ->with('adds', $adds)
      ->with('categoryName', $categoryName)
      ->with('categorySlug', $category);
  }

	/**
	 * Show the form for editing the specified add.
	 * GET /adds/{id}/edit
	 *
	 * @param $id
	 */
	public function edit($id)
	{

	}

	/**
	 * Update the specified add in storage.
	 * PUT /adds/{id}
	 *
	 * @param $id
	 */
	public function update($id)
	{

	}

	/**
	 * Remove the specified add from storage.
	 * DELETE /adds/{id}
	 *
	 * @param $id
	 */
	public function destroy($id)
	{
    Add::destroy($id);
    return redirect()->route('adds.my')
      ->with('status', 'Oglas je uspješno obrisan!');
	}

  /**
   * Returns set of adds for given search term.
   *
   * @return $this
   */
	public function search()
  {
    $term = Input::get('term');

    $validation = Validator::make(
      ['term' => $term],
      ['term' => 'required|string']
    );

    if($validation->fails()) {
      return redirect()->back();
    }

    $adds = $this->add->search($term);

    return view('adds.showCategory')
      ->with('adds', $adds)
      ->with('categoryName', 'Rezultati pretrage  ')
      ->with('categorySlug', 'search');
  }
}

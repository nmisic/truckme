<?php

use Illuminate\Database\Seeder;

class TruckCategoriesSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('truck_categories')->delete();

		DB::table('truck_categories')->insert(
			[
        0 => [
          'truck_category' => 'Sve kategorije',
          'truck_category_slug' => 'all'
        ],
				1 => [
					'truck_category' => 'Generalni prijevoz',
					'truck_category_slug' => 'general'
				],
				2 => [
					'truck_category' => 'Cisterne',
					'truck_category_slug' => 'cistern'
				],
				3 => [
					'truck_category' => 'Kiperi',
					'truck_category_slug' => 'tipper'
				],
				4 => [
					'truck_category' => 'Kontejneri',
					'truck_category_slug' => 'container'
				],
				5 => [
					'truck_category' => 'Hladnjače',
					'truck_category_slug' => 'refrigerated'
				],
				6 => [
					'truck_category' => 'Opasne tvari',
					'truck_category_slug' => 'hazard'
				],
				7 => [
					'truck_category' => 'Poseban prijevoz',
					'truck_category_slug' => 'special'
				],
				8 => [
					'truck_category' => 'Dizalice i kranovi',
					'truck_category_slug' => 'crane'
				],
				9 => [
					'truck_category' => 'Vučna služba',
					'truck_category_slug' => 'towing'
				],
				10 => [
					'truck_category' => 'Prijevoz plovila',
					'truck_category_slug' => 'vessel'
				]
			]
		);
	}
}

<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cities')->delete();
        
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 1,
                'city_name' => 'Andrijaševci',
            ),
            1 => 
            array (
                'id' => 2,
                'city_name' => 'Antunovac',
            ),
            2 => 
            array (
                'id' => 3,
                'city_name' => 'Babina Greda',
            ),
            3 => 
            array (
                'id' => 4,
                'city_name' => 'Bakar',
            ),
            4 => 
            array (
                'id' => 5,
                'city_name' => 'Bale-Valle',
            ),
            5 => 
            array (
                'id' => 6,
                'city_name' => 'Barban',
            ),
            6 => 
            array (
                'id' => 7,
                'city_name' => 'Barilović',
            ),
            7 => 
            array (
                'id' => 8,
                'city_name' => 'Baška',
            ),
            8 => 
            array (
                'id' => 9,
                'city_name' => 'Baška Voda',
            ),
            9 => 
            array (
                'id' => 10,
                'city_name' => 'Bebrina',
            ),
            10 => 
            array (
                'id' => 11,
                'city_name' => 'Bedekovčina',
            ),
            11 => 
            array (
                'id' => 12,
                'city_name' => 'Bedenica',
            ),
            12 => 
            array (
                'id' => 13,
                'city_name' => 'Bednja',
            ),
            13 => 
            array (
                'id' => 14,
                'city_name' => 'Beli Manastir',
            ),
            14 => 
            array (
                'id' => 15,
                'city_name' => 'Belica',
            ),
            15 => 
            array (
                'id' => 16,
                'city_name' => 'Belišće',
            ),
            16 => 
            array (
                'id' => 17,
                'city_name' => 'Benkovac',
            ),
            17 => 
            array (
                'id' => 18,
                'city_name' => 'Berek',
            ),
            18 => 
            array (
                'id' => 19,
                'city_name' => 'Beretinec',
            ),
            19 => 
            array (
                'id' => 20,
                'city_name' => 'Bibinje',
            ),
            20 => 
            array (
                'id' => 21,
                'city_name' => 'Bilice',
            ),
            21 => 
            array (
                'id' => 22,
                'city_name' => 'Bilje',
            ),
            22 => 
            array (
                'id' => 23,
                'city_name' => 'Biograd na Moru',
            ),
            23 => 
            array (
                'id' => 24,
                'city_name' => 'Biskupija',
            ),
            24 => 
            array (
                'id' => 25,
                'city_name' => 'Bistra',
            ),
            25 => 
            array (
                'id' => 26,
                'city_name' => 'Bizovac',
            ),
            26 => 
            array (
                'id' => 27,
                'city_name' => 'Bjelovar',
            ),
            27 => 
            array (
                'id' => 28,
                'city_name' => 'Blato',
            ),
            28 => 
            array (
                'id' => 29,
                'city_name' => 'Bogdanovci',
            ),
            29 => 
            array (
                'id' => 30,
                'city_name' => 'Bol',
            ),
            30 => 
            array (
                'id' => 31,
                'city_name' => 'Borovo',
            ),
            31 => 
            array (
                'id' => 32,
                'city_name' => 'Bosiljevo',
            ),
            32 => 
            array (
                'id' => 33,
                'city_name' => 'Bošnjaci',
            ),
            33 => 
            array (
                'id' => 34,
                'city_name' => 'Brckovljani',
            ),
            34 => 
            array (
                'id' => 35,
                'city_name' => 'Brdovec',
            ),
            35 => 
            array (
                'id' => 36,
                'city_name' => 'Brela',
            ),
            36 => 
            array (
                'id' => 37,
                'city_name' => 'Brestovac',
            ),
            37 => 
            array (
                'id' => 38,
                'city_name' => 'Breznica',
            ),
            38 => 
            array (
                'id' => 39,
                'city_name' => 'Breznički Hum',
            ),
            39 => 
            array (
                'id' => 40,
                'city_name' => 'Brinje',
            ),
            40 => 
            array (
                'id' => 41,
                'city_name' => 'Brod Moravice',
            ),
            41 => 
            array (
                'id' => 42,
                'city_name' => 'Brodski Stupnik',
            ),
            42 => 
            array (
                'id' => 43,
                'city_name' => 'Brtonigla-Verteneglio',
            ),
            43 => 
            array (
                'id' => 44,
                'city_name' => 'Budinščina',
            ),
            44 => 
            array (
                'id' => 45,
                'city_name' => 'Buje-Buie',
            ),
            45 => 
            array (
                'id' => 46,
                'city_name' => 'Bukovlje',
            ),
            46 => 
            array (
                'id' => 47,
                'city_name' => 'Buzet',
            ),
            47 => 
            array (
                'id' => 48,
                'city_name' => 'Cerna',
            ),
            48 => 
            array (
                'id' => 49,
                'city_name' => 'Cernik',
            ),
            49 => 
            array (
                'id' => 50,
                'city_name' => 'Cerovlje',
            ),
            50 => 
            array (
                'id' => 51,
                'city_name' => 'Cestica',
            ),
            51 => 
            array (
                'id' => 52,
                'city_name' => 'Cetingrad',
            ),
            52 => 
            array (
                'id' => 53,
                'city_name' => 'Cista Provo',
            ),
            53 => 
            array (
                'id' => 54,
                'city_name' => 'Civljane',
            ),
            54 => 
            array (
                'id' => 55,
                'city_name' => 'Cres',
            ),
            55 => 
            array (
                'id' => 56,
                'city_name' => 'Crikvenica',
            ),
            56 => 
            array (
                'id' => 57,
                'city_name' => 'Crnac',
            ),
            57 => 
            array (
                'id' => 58,
                'city_name' => 'Čabar',
            ),
            58 => 
            array (
                'id' => 59,
                'city_name' => 'Čačinci',
            ),
            59 => 
            array (
                'id' => 60,
                'city_name' => 'Čađavica',
            ),
            60 => 
            array (
                'id' => 61,
                'city_name' => 'Čaglin',
            ),
            61 => 
            array (
                'id' => 62,
                'city_name' => 'Čakovec',
            ),
            62 => 
            array (
                'id' => 63,
                'city_name' => 'Čavle',
            ),
            63 => 
            array (
                'id' => 64,
                'city_name' => 'Čazma',
            ),
            64 => 
            array (
                'id' => 65,
                'city_name' => 'Čeminac',
            ),
            65 => 
            array (
                'id' => 66,
                'city_name' => 'Čepin',
            ),
            66 => 
            array (
                'id' => 67,
                'city_name' => 'Darda',
            ),
            67 => 
            array (
                'id' => 68,
                'city_name' => 'Daruvar',
            ),
            68 => 
            array (
                'id' => 69,
                'city_name' => 'Davor',
            ),
            69 => 
            array (
                'id' => 70,
                'city_name' => 'Dekanovec',
            ),
            70 => 
            array (
                'id' => 71,
                'city_name' => 'Delnice',
            ),
            71 => 
            array (
                'id' => 72,
                'city_name' => 'Desinić',
            ),
            72 => 
            array (
                'id' => 73,
                'city_name' => 'Dežanovac',
            ),
            73 => 
            array (
                'id' => 74,
                'city_name' => 'Dicmo',
            ),
            74 => 
            array (
                'id' => 75,
                'city_name' => 'Dobrinj',
            ),
            75 => 
            array (
                'id' => 76,
                'city_name' => 'Domašinec',
            ),
            76 => 
            array (
                'id' => 77,
                'city_name' => 'Donja Dubrava',
            ),
            77 => 
            array (
                'id' => 78,
                'city_name' => 'Donja Motičina',
            ),
            78 => 
            array (
                'id' => 79,
                'city_name' => 'Donja Stubica',
            ),
            79 => 
            array (
                'id' => 80,
                'city_name' => 'Donja Voća',
            ),
            80 => 
            array (
                'id' => 81,
                'city_name' => 'Donji Andrijevci',
            ),
            81 => 
            array (
                'id' => 82,
                'city_name' => 'Donji Kraljevec',
            ),
            82 => 
            array (
                'id' => 83,
                'city_name' => 'Donji Kukuruzari',
            ),
            83 => 
            array (
                'id' => 84,
                'city_name' => 'Donji Lapac',
            ),
            84 => 
            array (
                'id' => 85,
                'city_name' => 'Donji Miholjac',
            ),
            85 => 
            array (
                'id' => 86,
                'city_name' => 'Donji Vidovec',
            ),
            86 => 
            array (
                'id' => 87,
                'city_name' => 'Dragalić',
            ),
            87 => 
            array (
                'id' => 88,
                'city_name' => 'Draganić',
            ),
            88 => 
            array (
                'id' => 89,
                'city_name' => 'Draž',
            ),
            89 => 
            array (
                'id' => 90,
                'city_name' => 'Drenovci',
            ),
            90 => 
            array (
                'id' => 91,
                'city_name' => 'Drenje',
            ),
            91 => 
            array (
                'id' => 92,
                'city_name' => 'Drniš',
            ),
            92 => 
            array (
                'id' => 93,
                'city_name' => 'Drnje',
            ),
            93 => 
            array (
                'id' => 94,
                'city_name' => 'Dubrava',
            ),
            94 => 
            array (
                'id' => 95,
                'city_name' => 'Dubravica',
            ),
            95 => 
            array (
                'id' => 96,
                'city_name' => 'Dubrovačko primorje',
            ),
            96 => 
            array (
                'id' => 97,
                'city_name' => 'Dubrovnik',
            ),
            97 => 
            array (
                'id' => 98,
                'city_name' => 'Duga Resa',
            ),
            98 => 
            array (
                'id' => 99,
                'city_name' => 'Dugi Rat',
            ),
            99 => 
            array (
                'id' => 100,
                'city_name' => 'Dugo Selo',
            ),
            100 => 
            array (
                'id' => 101,
                'city_name' => 'Dugopolje',
            ),
            101 => 
            array (
                'id' => 102,
                'city_name' => 'Dvor',
            ),
            102 => 
            array (
                'id' => 103,
                'city_name' => 'Đakovo',
            ),
            103 => 
            array (
                'id' => 104,
                'city_name' => 'Đelekovec',
            ),
            104 => 
            array (
                'id' => 105,
                'city_name' => 'Đulovac',
            ),
            105 => 
            array (
                'id' => 106,
                'city_name' => 'Đurđenovac',
            ),
            106 => 
            array (
                'id' => 107,
                'city_name' => 'Đurđevac',
            ),
            107 => 
            array (
                'id' => 108,
                'city_name' => 'Đurmanec',
            ),
            108 => 
            array (
                'id' => 109,
                'city_name' => 'Erdut',
            ),
            109 => 
            array (
                'id' => 110,
                'city_name' => 'Ernestinovo',
            ),
            110 => 
            array (
                'id' => 111,
                'city_name' => 'Ervenik',
            ),
            111 => 
            array (
                'id' => 112,
                'city_name' => 'Farkaševac',
            ),
            112 => 
            array (
                'id' => 113,
                'city_name' => 'Fažana-Fasana',
            ),
            113 => 
            array (
                'id' => 114,
                'city_name' => 'Ferdinandovac',
            ),
            114 => 
            array (
                'id' => 115,
                'city_name' => 'Feričanci',
            ),
            115 => 
            array (
                'id' => 116,
                'city_name' => 'Funtana-Fontane',
            ),
            116 => 
            array (
                'id' => 117,
                'city_name' => 'Fužine',
            ),
            117 => 
            array (
                'id' => 118,
                'city_name' => 'Galovac',
            ),
            118 => 
            array (
                'id' => 119,
                'city_name' => 'Garčin',
            ),
            119 => 
            array (
                'id' => 120,
                'city_name' => 'Garešnica',
            ),
            120 => 
            array (
                'id' => 121,
                'city_name' => 'Generalski Stol',
            ),
            121 => 
            array (
                'id' => 122,
                'city_name' => 'Glina',
            ),
            122 => 
            array (
                'id' => 123,
                'city_name' => 'Gola',
            ),
            123 => 
            array (
                'id' => 124,
                'city_name' => 'Goričan',
            ),
            124 => 
            array (
                'id' => 125,
                'city_name' => 'Gorjani',
            ),
            125 => 
            array (
                'id' => 126,
                'city_name' => 'Gornja Rijeka',
            ),
            126 => 
            array (
                'id' => 127,
                'city_name' => 'Gornja Stubica',
            ),
            127 => 
            array (
                'id' => 128,
                'city_name' => 'Gornja Vrba',
            ),
            128 => 
            array (
                'id' => 129,
                'city_name' => 'Gornji Bogićevci',
            ),
            129 => 
            array (
                'id' => 130,
                'city_name' => 'Gornji Kneginec',
            ),
            130 => 
            array (
                'id' => 131,
                'city_name' => 'Gornji Mihaljevec',
            ),
            131 => 
            array (
                'id' => 132,
                'city_name' => 'Gospić',
            ),
            132 => 
            array (
                'id' => 133,
                'city_name' => 'Gračac',
            ),
            133 => 
            array (
                'id' => 134,
                'city_name' => 'Gračišće',
            ),
            134 => 
            array (
                'id' => 135,
                'city_name' => 'Gradac',
            ),
            135 => 
            array (
                'id' => 136,
                'city_name' => 'Gradec',
            ),
            136 => 
            array (
                'id' => 137,
                'city_name' => 'Gradina',
            ),
            137 => 
            array (
                'id' => 138,
                'city_name' => 'Gradište',
            ),
            138 => 
            array (
                'id' => 139,
                'city_name' => 'Grožnjan-Grisignana',
            ),
            139 => 
            array (
                'id' => 140,
                'city_name' => 'Grubišno Polje',
            ),
            140 => 
            array (
                'id' => 141,
                'city_name' => 'Gundinci',
            ),
            141 => 
            array (
                'id' => 142,
                'city_name' => 'Gunja',
            ),
            142 => 
            array (
                'id' => 143,
                'city_name' => 'Gvozd',
            ),
            143 => 
            array (
                'id' => 144,
                'city_name' => 'Hercegovac',
            ),
            144 => 
            array (
                'id' => 145,
                'city_name' => 'Hlebine',
            ),
            145 => 
            array (
                'id' => 146,
                'city_name' => 'Hrašćina',
            ),
            146 => 
            array (
                'id' => 147,
                'city_name' => 'Hrvace',
            ),
            147 => 
            array (
                'id' => 148,
                'city_name' => 'Hrvatska Dubica',
            ),
            148 => 
            array (
                'id' => 149,
                'city_name' => 'Hrvatska Kostajnica',
            ),
            149 => 
            array (
                'id' => 150,
                'city_name' => 'Hum na Sutli',
            ),
            150 => 
            array (
                'id' => 151,
                'city_name' => 'Hvar',
            ),
            151 => 
            array (
                'id' => 152,
                'city_name' => 'Ilok',
            ),
            152 => 
            array (
                'id' => 153,
                'city_name' => 'Imotski',
            ),
            153 => 
            array (
                'id' => 154,
                'city_name' => 'Ivanec',
            ),
            154 => 
            array (
                'id' => 155,
                'city_name' => 'Ivanić-Grad',
            ),
            155 => 
            array (
                'id' => 156,
                'city_name' => 'Ivankovo',
            ),
            156 => 
            array (
                'id' => 157,
                'city_name' => 'Ivanska',
            ),
            157 => 
            array (
                'id' => 158,
                'city_name' => 'Jagodnjak',
            ),
            158 => 
            array (
                'id' => 159,
                'city_name' => 'Jakovlje',
            ),
            159 => 
            array (
                'id' => 160,
                'city_name' => 'Jakšić',
            ),
            160 => 
            array (
                'id' => 161,
                'city_name' => 'Jalžabet',
            ),
            161 => 
            array (
                'id' => 162,
                'city_name' => 'Janjina',
            ),
            162 => 
            array (
                'id' => 163,
                'city_name' => 'Jarmina',
            ),
            163 => 
            array (
                'id' => 164,
                'city_name' => 'Jasenice',
            ),
            164 => 
            array (
                'id' => 165,
                'city_name' => 'Jasenovac',
            ),
            165 => 
            array (
                'id' => 166,
                'city_name' => 'Jastrebarsko',
            ),
            166 => 
            array (
                'id' => 167,
                'city_name' => 'Jelenje',
            ),
            167 => 
            array (
                'id' => 168,
                'city_name' => 'Jelsa',
            ),
            168 => 
            array (
                'id' => 169,
                'city_name' => 'Jesenje',
            ),
            169 => 
            array (
                'id' => 170,
                'city_name' => 'Josipdol',
            ),
            170 => 
            array (
                'id' => 171,
                'city_name' => 'Kali',
            ),
            171 => 
            array (
                'id' => 172,
                'city_name' => 'Kalinovac',
            ),
            172 => 
            array (
                'id' => 173,
                'city_name' => 'Kalnik',
            ),
            173 => 
            array (
                'id' => 174,
                'city_name' => 'Kamanje',
            ),
            174 => 
            array (
                'id' => 175,
                'city_name' => 'Kanfanar',
            ),
            175 => 
            array (
                'id' => 176,
                'city_name' => 'Kapela',
            ),
            176 => 
            array (
                'id' => 177,
                'city_name' => 'Kaptol',
            ),
            177 => 
            array (
                'id' => 178,
                'city_name' => 'Karlobag',
            ),
            178 => 
            array (
                'id' => 179,
                'city_name' => 'Karlovac',
            ),
            179 => 
            array (
                'id' => 180,
                'city_name' => 'Karojba',
            ),
            180 => 
            array (
                'id' => 181,
                'city_name' => 'Kastav',
            ),
            181 => 
            array (
                'id' => 182,
                'city_name' => 'Kaštela',
            ),
            182 => 
            array (
                'id' => 183,
                'city_name' => 'Kaštelir-Labinci-Castelliere-S. Domenica',
            ),
            183 => 
            array (
                'id' => 184,
                'city_name' => 'Kijevo',
            ),
            184 => 
            array (
                'id' => 185,
                'city_name' => 'Kistanje',
            ),
            185 => 
            array (
                'id' => 186,
                'city_name' => 'Klakar',
            ),
            186 => 
            array (
                'id' => 187,
                'city_name' => 'Klana',
            ),
            187 => 
            array (
                'id' => 188,
                'city_name' => 'Klanjec',
            ),
            188 => 
            array (
                'id' => 189,
                'city_name' => 'Klenovnik',
            ),
            189 => 
            array (
                'id' => 190,
                'city_name' => 'Klinča Sela',
            ),
            190 => 
            array (
                'id' => 191,
                'city_name' => 'Klis',
            ),
            191 => 
            array (
                'id' => 192,
                'city_name' => 'Kloštar Ivanić',
            ),
            192 => 
            array (
                'id' => 193,
                'city_name' => 'Kloštar Podravski',
            ),
            193 => 
            array (
                'id' => 194,
                'city_name' => 'Kneževi Vinogradi',
            ),
            194 => 
            array (
                'id' => 195,
                'city_name' => 'Knin',
            ),
            195 => 
            array (
                'id' => 196,
                'city_name' => 'Kolan',
            ),
            196 => 
            array (
                'id' => 197,
                'city_name' => 'Komiža',
            ),
            197 => 
            array (
                'id' => 198,
                'city_name' => 'Konavle',
            ),
            198 => 
            array (
                'id' => 199,
                'city_name' => 'Končanica',
            ),
            199 => 
            array (
                'id' => 200,
                'city_name' => 'Konjščina',
            ),
            200 => 
            array (
                'id' => 201,
                'city_name' => 'Koprivnica',
            ),
            201 => 
            array (
                'id' => 202,
                'city_name' => 'Koprivnički Bregi',
            ),
            202 => 
            array (
                'id' => 203,
                'city_name' => 'Koprivnički Ivanec',
            ),
            203 => 
            array (
                'id' => 204,
                'city_name' => 'Korčula',
            ),
            204 => 
            array (
                'id' => 205,
                'city_name' => 'Kostrena',
            ),
            205 => 
            array (
                'id' => 206,
                'city_name' => 'Koška',
            ),
            206 => 
            array (
                'id' => 207,
                'city_name' => 'Kotoriba',
            ),
            207 => 
            array (
                'id' => 208,
                'city_name' => 'Kraljevec na Sutli',
            ),
            208 => 
            array (
                'id' => 209,
                'city_name' => 'Kraljevica',
            ),
            209 => 
            array (
                'id' => 210,
                'city_name' => 'Krapina',
            ),
            210 => 
            array (
                'id' => 211,
                'city_name' => 'Krapinske Toplice',
            ),
            211 => 
            array (
                'id' => 212,
                'city_name' => 'Krašić',
            ),
            212 => 
            array (
                'id' => 213,
                'city_name' => 'Kravarsko',
            ),
            213 => 
            array (
                'id' => 214,
                'city_name' => 'Križ',
            ),
            214 => 
            array (
                'id' => 215,
                'city_name' => 'Križevci',
            ),
            215 => 
            array (
                'id' => 216,
                'city_name' => 'Krk',
            ),
            216 => 
            array (
                'id' => 217,
                'city_name' => 'Krnjak',
            ),
            217 => 
            array (
                'id' => 218,
                'city_name' => 'Kršan',
            ),
            218 => 
            array (
                'id' => 219,
                'city_name' => 'Kukljica',
            ),
            219 => 
            array (
                'id' => 220,
                'city_name' => 'Kula Norinska',
            ),
            220 => 
            array (
                'id' => 221,
                'city_name' => 'Kumrovec',
            ),
            221 => 
            array (
                'id' => 222,
                'city_name' => 'Kutina',
            ),
            222 => 
            array (
                'id' => 223,
                'city_name' => 'Kutjevo',
            ),
            223 => 
            array (
                'id' => 224,
                'city_name' => 'Labin',
            ),
            224 => 
            array (
                'id' => 225,
                'city_name' => 'Lanišće',
            ),
            225 => 
            array (
                'id' => 226,
                'city_name' => 'Lasinja',
            ),
            226 => 
            array (
                'id' => 227,
                'city_name' => 'Lastovo',
            ),
            227 => 
            array (
                'id' => 228,
                'city_name' => 'Lećevica',
            ),
            228 => 
            array (
                'id' => 229,
                'city_name' => 'Legrad',
            ),
            229 => 
            array (
                'id' => 230,
                'city_name' => 'Lekenik',
            ),
            230 => 
            array (
                'id' => 231,
                'city_name' => 'Lepoglava',
            ),
            231 => 
            array (
                'id' => 232,
                'city_name' => 'Levanjska Varoš',
            ),
            232 => 
            array (
                'id' => 233,
                'city_name' => 'Lipik',
            ),
            233 => 
            array (
                'id' => 234,
                'city_name' => 'Lipovljani',
            ),
            234 => 
            array (
                'id' => 235,
                'city_name' => 'Lišane Ostrovičke',
            ),
            235 => 
            array (
                'id' => 236,
                'city_name' => 'Ližnjan-Lisignano',
            ),
            236 => 
            array (
                'id' => 237,
                'city_name' => 'Lobor',
            ),
            237 => 
            array (
                'id' => 238,
                'city_name' => 'Lokve',
            ),
            238 => 
            array (
                'id' => 239,
                'city_name' => 'Lokvičići',
            ),
            239 => 
            array (
                'id' => 240,
                'city_name' => 'Lopar',
            ),
            240 => 
            array (
                'id' => 241,
                'city_name' => 'Lovas',
            ),
            241 => 
            array (
                'id' => 242,
                'city_name' => 'Lovinac',
            ),
            242 => 
            array (
                'id' => 243,
                'city_name' => 'Lovran',
            ),
            243 => 
            array (
                'id' => 244,
                'city_name' => 'Lovreć',
            ),
            244 => 
            array (
                'id' => 245,
                'city_name' => 'Ludbreg',
            ),
            245 => 
            array (
                'id' => 246,
                'city_name' => 'Luka',
            ),
            246 => 
            array (
                'id' => 247,
                'city_name' => 'Lukač',
            ),
            247 => 
            array (
                'id' => 248,
                'city_name' => 'Lumbarda',
            ),
            248 => 
            array (
                'id' => 249,
                'city_name' => 'Lupoglav',
            ),
            249 => 
            array (
                'id' => 250,
                'city_name' => 'Ljubešćica',
            ),
            250 => 
            array (
                'id' => 251,
                'city_name' => 'Mače',
            ),
            251 => 
            array (
                'id' => 252,
                'city_name' => 'Magadenovac',
            ),
            252 => 
            array (
                'id' => 253,
                'city_name' => 'Majur',
            ),
            253 => 
            array (
                'id' => 254,
                'city_name' => 'Makarska',
            ),
            254 => 
            array (
                'id' => 255,
                'city_name' => 'Mala Subotica',
            ),
            255 => 
            array (
                'id' => 256,
                'city_name' => 'Mali Bukovec',
            ),
            256 => 
            array (
                'id' => 257,
                'city_name' => 'Mali Lošinj',
            ),
            257 => 
            array (
                'id' => 258,
                'city_name' => 'Malinska-Dubašnica',
            ),
            258 => 
            array (
                'id' => 259,
                'city_name' => 'Marčana',
            ),
            259 => 
            array (
                'id' => 260,
                'city_name' => 'Marija Bistrica',
            ),
            260 => 
            array (
                'id' => 261,
                'city_name' => 'Marija Gorica',
            ),
            261 => 
            array (
                'id' => 262,
                'city_name' => 'Marijanci',
            ),
            262 => 
            array (
                'id' => 263,
                'city_name' => 'Marina',
            ),
            263 => 
            array (
                'id' => 264,
                'city_name' => 'Markušica',
            ),
            264 => 
            array (
                'id' => 265,
                'city_name' => 'Martijanec',
            ),
            265 => 
            array (
                'id' => 266,
                'city_name' => 'Martinska Ves',
            ),
            266 => 
            array (
                'id' => 267,
                'city_name' => 'Maruševec',
            ),
            267 => 
            array (
                'id' => 268,
                'city_name' => 'Matulji',
            ),
            268 => 
            array (
                'id' => 269,
                'city_name' => 'Medulin',
            ),
            269 => 
            array (
                'id' => 270,
                'city_name' => 'Metković',
            ),
            270 => 
            array (
                'id' => 271,
                'city_name' => 'Mihovljan',
            ),
            271 => 
            array (
                'id' => 272,
                'city_name' => 'Mikleuš',
            ),
            272 => 
            array (
                'id' => 273,
                'city_name' => 'Milna',
            ),
            273 => 
            array (
                'id' => 274,
                'city_name' => 'Mljet',
            ),
            274 => 
            array (
                'id' => 275,
                'city_name' => 'Molve',
            ),
            275 => 
            array (
                'id' => 276,
                'city_name' => 'Mošćenička Draga',
            ),
            276 => 
            array (
                'id' => 277,
                'city_name' => 'Motovun-Montona',
            ),
            277 => 
            array (
                'id' => 278,
                'city_name' => 'Mrkopalj',
            ),
            278 => 
            array (
                'id' => 279,
                'city_name' => 'Muć',
            ),
            279 => 
            array (
                'id' => 280,
                'city_name' => 'Mursko Središće',
            ),
            280 => 
            array (
                'id' => 281,
                'city_name' => 'Murter-Kornati',
            ),
            281 => 
            array (
                'id' => 282,
                'city_name' => 'Našice',
            ),
            282 => 
            array (
                'id' => 283,
                'city_name' => 'Nedelišće',
            ),
            283 => 
            array (
                'id' => 284,
                'city_name' => 'Negoslavci',
            ),
            284 => 
            array (
                'id' => 285,
                'city_name' => 'Nerežišća',
            ),
            285 => 
            array (
                'id' => 286,
                'city_name' => 'Netretić',
            ),
            286 => 
            array (
                'id' => 287,
                'city_name' => 'Nijemci',
            ),
            287 => 
            array (
                'id' => 288,
                'city_name' => 'Nin',
            ),
            288 => 
            array (
                'id' => 289,
                'city_name' => 'Nova Bukovica',
            ),
            289 => 
            array (
                'id' => 290,
                'city_name' => 'Nova Gradiška',
            ),
            290 => 
            array (
                'id' => 291,
                'city_name' => 'Nova Kapela',
            ),
            291 => 
            array (
                'id' => 292,
                'city_name' => 'Nova Rača',
            ),
            292 => 
            array (
                'id' => 293,
                'city_name' => 'Novalja',
            ),
            293 => 
            array (
                'id' => 294,
                'city_name' => 'Novi Golubovec',
            ),
            294 => 
            array (
                'id' => 295,
                'city_name' => 'Novi Marof',
            ),
            295 => 
            array (
                'id' => 296,
                'city_name' => 'Novi Vinodolski',
            ),
            296 => 
            array (
                'id' => 297,
                'city_name' => 'Novigrad',
            ),
            297 => 
            array (
                'id' => 298,
                'city_name' => 'Novigrad Podravski',
            ),
            298 => 
            array (
                'id' => 299,
                'city_name' => 'Novigrad-Cittanova',
            ),
            299 => 
            array (
                'id' => 300,
                'city_name' => 'Novo Virje',
            ),
            300 => 
            array (
                'id' => 301,
                'city_name' => 'Novska',
            ),
            301 => 
            array (
                'id' => 302,
                'city_name' => 'Nuštar',
            ),
            302 => 
            array (
                'id' => 303,
                'city_name' => 'Obrovac',
            ),
            303 => 
            array (
                'id' => 304,
                'city_name' => 'Ogulin',
            ),
            304 => 
            array (
                'id' => 305,
                'city_name' => 'Okrug',
            ),
            305 => 
            array (
                'id' => 306,
                'city_name' => 'Okučani',
            ),
            306 => 
            array (
                'id' => 307,
                'city_name' => 'Omiš',
            ),
            307 => 
            array (
                'id' => 308,
                'city_name' => 'Omišalj',
            ),
            308 => 
            array (
                'id' => 309,
                'city_name' => 'Opatija',
            ),
            309 => 
            array (
                'id' => 310,
                'city_name' => 'Oprisavci',
            ),
            310 => 
            array (
                'id' => 311,
                'city_name' => 'Oprtalj-Portole',
            ),
            311 => 
            array (
                'id' => 312,
                'city_name' => 'Opuzen',
            ),
            312 => 
            array (
                'id' => 313,
                'city_name' => 'Orahovica',
            ),
            313 => 
            array (
                'id' => 314,
                'city_name' => 'Orebić',
            ),
            314 => 
            array (
                'id' => 315,
                'city_name' => 'Orehovica',
            ),
            315 => 
            array (
                'id' => 316,
                'city_name' => 'Oriovac',
            ),
            316 => 
            array (
                'id' => 317,
                'city_name' => 'Orle',
            ),
            317 => 
            array (
                'id' => 318,
                'city_name' => 'Oroslavje',
            ),
            318 => 
            array (
                'id' => 319,
                'city_name' => 'Osijek',
            ),
            319 => 
            array (
                'id' => 320,
                'city_name' => 'Otočac',
            ),
            320 => 
            array (
                'id' => 321,
                'city_name' => 'Otok',
            ),
            321 => 
            array (
                'id' => 322,
                'city_name' => 'Otok',
            ),
            322 => 
            array (
                'id' => 323,
                'city_name' => 'Ozalj',
            ),
            323 => 
            array (
                'id' => 324,
                'city_name' => 'Pag',
            ),
            324 => 
            array (
                'id' => 325,
                'city_name' => 'Pakoštane',
            ),
            325 => 
            array (
                'id' => 326,
                'city_name' => 'Pakrac',
            ),
            326 => 
            array (
                'id' => 327,
                'city_name' => 'Pašman',
            ),
            327 => 
            array (
                'id' => 328,
                'city_name' => 'Pazin',
            ),
            328 => 
            array (
                'id' => 329,
                'city_name' => 'Perušić',
            ),
            329 => 
            array (
                'id' => 330,
                'city_name' => 'Peteranec',
            ),
            330 => 
            array (
                'id' => 331,
                'city_name' => 'Petlovac',
            ),
            331 => 
            array (
                'id' => 332,
                'city_name' => 'Petrijanec',
            ),
            332 => 
            array (
                'id' => 333,
                'city_name' => 'Petrijevci',
            ),
            333 => 
            array (
                'id' => 334,
                'city_name' => 'Petrinja',
            ),
            334 => 
            array (
                'id' => 335,
                'city_name' => 'Petrovsko',
            ),
            335 => 
            array (
                'id' => 336,
                'city_name' => 'Pićan',
            ),
            336 => 
            array (
                'id' => 337,
                'city_name' => 'Pirovac',
            ),
            337 => 
            array (
                'id' => 338,
                'city_name' => 'Pisarovina',
            ),
            338 => 
            array (
                'id' => 339,
                'city_name' => 'Pitomača',
            ),
            339 => 
            array (
                'id' => 340,
                'city_name' => 'Plaški',
            ),
            340 => 
            array (
                'id' => 341,
                'city_name' => 'Pleternica',
            ),
            341 => 
            array (
                'id' => 342,
                'city_name' => 'Plitvička Jezera',
            ),
            342 => 
            array (
                'id' => 343,
                'city_name' => 'Ploče',
            ),
            343 => 
            array (
                'id' => 344,
                'city_name' => 'Podbablje',
            ),
            344 => 
            array (
                'id' => 345,
                'city_name' => 'Podcrkavlje',
            ),
            345 => 
            array (
                'id' => 346,
                'city_name' => 'Podgora',
            ),
            346 => 
            array (
                'id' => 347,
                'city_name' => 'Podgorač',
            ),
            347 => 
            array (
                'id' => 348,
                'city_name' => 'Podravska Moslavina',
            ),
            348 => 
            array (
                'id' => 349,
                'city_name' => 'Podravske Sesvete',
            ),
            349 => 
            array (
                'id' => 350,
                'city_name' => 'Podstrana',
            ),
            350 => 
            array (
                'id' => 351,
                'city_name' => 'Podturen',
            ),
            351 => 
            array (
                'id' => 352,
                'city_name' => 'Pojezerje',
            ),
            352 => 
            array (
                'id' => 353,
                'city_name' => 'Pokupsko',
            ),
            353 => 
            array (
                'id' => 354,
                'city_name' => 'Polača',
            ),
            354 => 
            array (
                'id' => 355,
                'city_name' => 'Poličnik',
            ),
            355 => 
            array (
                'id' => 356,
                'city_name' => 'Popovac',
            ),
            356 => 
            array (
                'id' => 357,
                'city_name' => 'Popovača',
            ),
            357 => 
            array (
                'id' => 358,
                'city_name' => 'Poreč-Parenzo',
            ),
            358 => 
            array (
                'id' => 359,
                'city_name' => 'Posedarje',
            ),
            359 => 
            array (
                'id' => 360,
                'city_name' => 'Postira',
            ),
            360 => 
            array (
                'id' => 361,
                'city_name' => 'Povljana',
            ),
            361 => 
            array (
                'id' => 362,
                'city_name' => 'Požega',
            ),
            362 => 
            array (
                'id' => 363,
                'city_name' => 'Pregrada',
            ),
            363 => 
            array (
                'id' => 364,
                'city_name' => 'Preko',
            ),
            364 => 
            array (
                'id' => 365,
                'city_name' => 'Prelog',
            ),
            365 => 
            array (
                'id' => 366,
                'city_name' => 'Preseka',
            ),
            366 => 
            array (
                'id' => 367,
                'city_name' => 'Prgomet',
            ),
            367 => 
            array (
                'id' => 368,
                'city_name' => 'Pribislavec',
            ),
            368 => 
            array (
                'id' => 369,
                'city_name' => 'Primorski Dolac',
            ),
            369 => 
            array (
                'id' => 370,
                'city_name' => 'Primošten',
            ),
            370 => 
            array (
                'id' => 371,
                'city_name' => 'Privlaka',
            ),
            371 => 
            array (
                'id' => 372,
                'city_name' => 'Privlaka',
            ),
            372 => 
            array (
                'id' => 373,
                'city_name' => 'Proložac',
            ),
            373 => 
            array (
                'id' => 374,
                'city_name' => 'Promina',
            ),
            374 => 
            array (
                'id' => 375,
                'city_name' => 'Pučišća',
            ),
            375 => 
            array (
                'id' => 376,
                'city_name' => 'Pula-Pola',
            ),
            376 => 
            array (
                'id' => 377,
                'city_name' => 'Punat',
            ),
            377 => 
            array (
                'id' => 378,
                'city_name' => 'Punitovci',
            ),
            378 => 
            array (
                'id' => 379,
                'city_name' => 'Pušća',
            ),
            379 => 
            array (
                'id' => 380,
                'city_name' => 'Rab',
            ),
            380 => 
            array (
                'id' => 381,
                'city_name' => 'Radoboj',
            ),
            381 => 
            array (
                'id' => 382,
                'city_name' => 'Rakovec',
            ),
            382 => 
            array (
                'id' => 383,
                'city_name' => 'Rakovica',
            ),
            383 => 
            array (
                'id' => 384,
                'city_name' => 'Rasinja',
            ),
            384 => 
            array (
                'id' => 385,
                'city_name' => 'Raša',
            ),
            385 => 
            array (
                'id' => 386,
                'city_name' => 'Ravna Gora',
            ),
            386 => 
            array (
                'id' => 387,
                'city_name' => 'Ražanac',
            ),
            387 => 
            array (
                'id' => 388,
                'city_name' => 'Rešetari',
            ),
            388 => 
            array (
                'id' => 389,
                'city_name' => 'Ribnik',
            ),
            389 => 
            array (
                'id' => 390,
                'city_name' => 'Rijeka',
            ),
            390 => 
            array (
                'id' => 391,
                'city_name' => 'Rogoznica',
            ),
            391 => 
            array (
                'id' => 392,
                'city_name' => 'Rovinj-Rovigno',
            ),
            392 => 
            array (
                'id' => 393,
                'city_name' => 'Rovišće',
            ),
            393 => 
            array (
                'id' => 394,
                'city_name' => 'Rugvica',
            ),
            394 => 
            array (
                'id' => 395,
                'city_name' => 'Runovići',
            ),
            395 => 
            array (
                'id' => 396,
                'city_name' => 'Ružić',
            ),
            396 => 
            array (
                'id' => 397,
                'city_name' => 'Saborsko',
            ),
            397 => 
            array (
                'id' => 398,
                'city_name' => 'Sali',
            ),
            398 => 
            array (
                'id' => 399,
                'city_name' => 'Samobor',
            ),
            399 => 
            array (
                'id' => 400,
                'city_name' => 'Satnica Đakovačka',
            ),
            400 => 
            array (
                'id' => 401,
                'city_name' => 'Seget',
            ),
            401 => 
            array (
                'id' => 402,
                'city_name' => 'Selca',
            ),
            402 => 
            array (
                'id' => 403,
                'city_name' => 'Selnica',
            ),
            403 => 
            array (
                'id' => 404,
                'city_name' => 'Semeljci',
            ),
            404 => 
            array (
                'id' => 405,
                'city_name' => 'Senj',
            ),
            405 => 
            array (
                'id' => 406,
                'city_name' => 'Severin',
            ),
            406 => 
            array (
                'id' => 407,
                'city_name' => 'Sibinj',
            ),
            407 => 
            array (
                'id' => 408,
                'city_name' => 'Sikirevci',
            ),
            408 => 
            array (
                'id' => 409,
                'city_name' => 'Sinj',
            ),
            409 => 
            array (
                'id' => 410,
                'city_name' => 'Sirač',
            ),
            410 => 
            array (
                'id' => 411,
                'city_name' => 'Sisak',
            ),
            411 => 
            array (
                'id' => 412,
                'city_name' => 'Skrad',
            ),
            412 => 
            array (
                'id' => 413,
                'city_name' => 'Skradin',
            ),
            413 => 
            array (
                'id' => 414,
                'city_name' => 'Slatina',
            ),
            414 => 
            array (
                'id' => 415,
                'city_name' => 'Slavonski Brod',
            ),
            415 => 
            array (
                'id' => 416,
                'city_name' => 'Slavonski Šamac',
            ),
            416 => 
            array (
                'id' => 417,
                'city_name' => 'Slivno',
            ),
            417 => 
            array (
                'id' => 418,
                'city_name' => 'Slunj',
            ),
            418 => 
            array (
                'id' => 419,
                'city_name' => 'Smokvica',
            ),
            419 => 
            array (
                'id' => 420,
                'city_name' => 'Sokolovac',
            ),
            420 => 
            array (
                'id' => 421,
                'city_name' => 'Solin',
            ),
            421 => 
            array (
                'id' => 422,
                'city_name' => 'Sopje',
            ),
            422 => 
            array (
                'id' => 423,
                'city_name' => 'Split',
            ),
            423 => 
            array (
                'id' => 424,
                'city_name' => 'Sračinec',
            ),
            424 => 
            array (
                'id' => 425,
                'city_name' => 'Stankovci',
            ),
            425 => 
            array (
                'id' => 426,
                'city_name' => 'Stara Gradiška',
            ),
            426 => 
            array (
                'id' => 427,
                'city_name' => 'Stari Grad',
            ),
            427 => 
            array (
                'id' => 428,
                'city_name' => 'Stari Jankovci',
            ),
            428 => 
            array (
                'id' => 429,
                'city_name' => 'Stari Mikanovci',
            ),
            429 => 
            array (
                'id' => 430,
                'city_name' => 'Starigrad',
            ),
            430 => 
            array (
                'id' => 431,
                'city_name' => 'Staro Petrovo Selo',
            ),
            431 => 
            array (
                'id' => 432,
                'city_name' => 'Ston',
            ),
            432 => 
            array (
                'id' => 433,
                'city_name' => 'Strahoninec',
            ),
            433 => 
            array (
                'id' => 434,
                'city_name' => 'Strizivojna',
            ),
            434 => 
            array (
                'id' => 435,
                'city_name' => 'Stubičke Toplice',
            ),
            435 => 
            array (
                'id' => 436,
                'city_name' => 'Stupnik',
            ),
            436 => 
            array (
                'id' => 437,
                'city_name' => 'Sućuraj',
            ),
            437 => 
            array (
                'id' => 438,
                'city_name' => 'Suhopolje',
            ),
            438 => 
            array (
                'id' => 439,
                'city_name' => 'Sukošan',
            ),
            439 => 
            array (
                'id' => 440,
                'city_name' => 'Sunja',
            ),
            440 => 
            array (
                'id' => 441,
                'city_name' => 'Supetar',
            ),
            441 => 
            array (
                'id' => 442,
                'city_name' => 'Sutivan',
            ),
            442 => 
            array (
                'id' => 443,
                'city_name' => 'Sveta Marija',
            ),
            443 => 
            array (
                'id' => 444,
                'city_name' => 'Sveta Nedelja',
            ),
            444 => 
            array (
                'id' => 445,
                'city_name' => 'Sveta Nedelja',
            ),
            445 => 
            array (
                'id' => 446,
                'city_name' => 'Sveti Đurđ',
            ),
            446 => 
            array (
                'id' => 447,
                'city_name' => 'Sveti Filip i Jakov',
            ),
            447 => 
            array (
                'id' => 448,
                'city_name' => 'Sveti Ilija',
            ),
            448 => 
            array (
                'id' => 449,
                'city_name' => 'Sveti Ivan Zelina',
            ),
            449 => 
            array (
                'id' => 450,
                'city_name' => 'Sveti Ivan Žabno',
            ),
            450 => 
            array (
                'id' => 451,
                'city_name' => 'Sveti Juraj na Bregu',
            ),
            451 => 
            array (
                'id' => 452,
                'city_name' => 'Sveti Križ Začretje',
            ),
            452 => 
            array (
                'id' => 453,
                'city_name' => 'Sveti Lovreč',
            ),
            453 => 
            array (
                'id' => 454,
                'city_name' => 'Sveti Martin na Muri',
            ),
            454 => 
            array (
                'id' => 455,
                'city_name' => 'Sveti Petar Orehovec',
            ),
            455 => 
            array (
                'id' => 456,
                'city_name' => 'Sveti Petar u Šumi',
            ),
            456 => 
            array (
                'id' => 457,
                'city_name' => 'Svetvinčenat',
            ),
            457 => 
            array (
                'id' => 458,
                'city_name' => 'Šandrovac',
            ),
            458 => 
            array (
                'id' => 459,
                'city_name' => 'Šenkovec',
            ),
            459 => 
            array (
                'id' => 460,
                'city_name' => 'Šestanovac',
            ),
            460 => 
            array (
                'id' => 461,
                'city_name' => 'Šibenik',
            ),
            461 => 
            array (
                'id' => 462,
                'city_name' => 'Škabrnja',
            ),
            462 => 
            array (
                'id' => 463,
                'city_name' => 'Šodolovci',
            ),
            463 => 
            array (
                'id' => 464,
                'city_name' => 'Šolta',
            ),
            464 => 
            array (
                'id' => 465,
                'city_name' => 'Špišić Bukovica',
            ),
            465 => 
            array (
                'id' => 466,
                'city_name' => 'Štefanje',
            ),
            466 => 
            array (
                'id' => 467,
                'city_name' => 'Štitar',
            ),
            467 => 
            array (
                'id' => 468,
                'city_name' => 'Štrigova',
            ),
            468 => 
            array (
                'id' => 469,
                'city_name' => 'Tar-Vabriga-Torre Abrega',
            ),
            469 => 
            array (
                'id' => 470,
                'city_name' => 'Tinjan',
            ),
            470 => 
            array (
                'id' => 471,
                'city_name' => 'Tisno',
            ),
            471 => 
            array (
                'id' => 472,
                'city_name' => 'Tkon',
            ),
            472 => 
            array (
                'id' => 473,
                'city_name' => 'Tompojevci',
            ),
            473 => 
            array (
                'id' => 474,
                'city_name' => 'Topusko',
            ),
            474 => 
            array (
                'id' => 475,
                'city_name' => 'Tordinci',
            ),
            475 => 
            array (
                'id' => 476,
                'city_name' => 'Tounj',
            ),
            476 => 
            array (
                'id' => 477,
                'city_name' => 'Tovarnik',
            ),
            477 => 
            array (
                'id' => 478,
                'city_name' => 'Tribunj',
            ),
            478 => 
            array (
                'id' => 479,
                'city_name' => 'Trilj',
            ),
            479 => 
            array (
                'id' => 480,
                'city_name' => 'Trnava',
            ),
            480 => 
            array (
                'id' => 481,
                'city_name' => 'Trnovec Bartolovečki',
            ),
            481 => 
            array (
                'id' => 482,
                'city_name' => 'Trogir',
            ),
            482 => 
            array (
                'id' => 483,
                'city_name' => 'Trpanj',
            ),
            483 => 
            array (
                'id' => 484,
                'city_name' => 'Trpinja',
            ),
            484 => 
            array (
                'id' => 485,
                'city_name' => 'Tučepi',
            ),
            485 => 
            array (
                'id' => 486,
                'city_name' => 'Tuhelj',
            ),
            486 => 
            array (
                'id' => 487,
                'city_name' => 'Udbina',
            ),
            487 => 
            array (
                'id' => 488,
                'city_name' => 'Umag-Umago',
            ),
            488 => 
            array (
                'id' => 489,
                'city_name' => 'Unešić',
            ),
            489 => 
            array (
                'id' => 490,
                'city_name' => 'Valpovo',
            ),
            490 => 
            array (
                'id' => 491,
                'city_name' => 'Varaždin',
            ),
            491 => 
            array (
                'id' => 492,
                'city_name' => 'Varaždinske Toplice',
            ),
            492 => 
            array (
                'id' => 493,
                'city_name' => 'Vela Luka',
            ),
            493 => 
            array (
                'id' => 494,
                'city_name' => 'Velika',
            ),
            494 => 
            array (
                'id' => 495,
                'city_name' => 'Velika Gorica',
            ),
            495 => 
            array (
                'id' => 496,
                'city_name' => 'Velika Kopanica',
            ),
            496 => 
            array (
                'id' => 497,
                'city_name' => 'Velika Ludina',
            ),
            497 => 
            array (
                'id' => 498,
                'city_name' => 'Velika Pisanica',
            ),
            498 => 
            array (
                'id' => 499,
                'city_name' => 'Velika Trnovitica',
            ),
            499 => 
            array (
                'id' => 500,
                'city_name' => 'Veliki Bukovec',
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 501,
                'city_name' => 'Veliki Grđevac',
            ),
            1 => 
            array (
                'id' => 502,
                'city_name' => 'Veliko Trgovišće',
            ),
            2 => 
            array (
                'id' => 503,
                'city_name' => 'Veliko Trojstvo',
            ),
            3 => 
            array (
                'id' => 504,
                'city_name' => 'Vidovec',
            ),
            4 => 
            array (
                'id' => 505,
                'city_name' => 'Viljevo',
            ),
            5 => 
            array (
                'id' => 506,
                'city_name' => 'Vinica',
            ),
            6 => 
            array (
                'id' => 507,
                'city_name' => 'Vinkovci',
            ),
            7 => 
            array (
                'id' => 508,
                'city_name' => 'Vinodolska općina',
            ),
            8 => 
            array (
                'id' => 509,
                'city_name' => 'Vir',
            ),
            9 => 
            array (
                'id' => 510,
                'city_name' => 'Virje',
            ),
            10 => 
            array (
                'id' => 511,
                'city_name' => 'Virovitica',
            ),
            11 => 
            array (
                'id' => 512,
                'city_name' => 'Vis',
            ),
            12 => 
            array (
                'id' => 513,
                'city_name' => 'Visoko',
            ),
            13 => 
            array (
                'id' => 514,
                'city_name' => 'Viškovci',
            ),
            14 => 
            array (
                'id' => 515,
                'city_name' => 'Viškovo',
            ),
            15 => 
            array (
                'id' => 516,
                'city_name' => 'Višnjan-Visignano',
            ),
            16 => 
            array (
                'id' => 517,
                'city_name' => 'Vižinada-Visinada',
            ),
            17 => 
            array (
                'id' => 518,
                'city_name' => 'Vladislavci',
            ),
            18 => 
            array (
                'id' => 519,
                'city_name' => 'Voćin',
            ),
            19 => 
            array (
                'id' => 520,
                'city_name' => 'Vodice',
            ),
            20 => 
            array (
                'id' => 521,
                'city_name' => 'Vodnjan-Dignano',
            ),
            21 => 
            array (
                'id' => 522,
                'city_name' => 'Vođinci',
            ),
            22 => 
            array (
                'id' => 523,
                'city_name' => 'Vojnić',
            ),
            23 => 
            array (
                'id' => 524,
                'city_name' => 'Vratišinec',
            ),
            24 => 
            array (
                'id' => 525,
                'city_name' => 'Vrbanja',
            ),
            25 => 
            array (
                'id' => 526,
                'city_name' => 'Vrbje',
            ),
            26 => 
            array (
                'id' => 527,
                'city_name' => 'Vrbnik',
            ),
            27 => 
            array (
                'id' => 528,
                'city_name' => 'Vrbovec',
            ),
            28 => 
            array (
                'id' => 529,
                'city_name' => 'Vrbovsko',
            ),
            29 => 
            array (
                'id' => 530,
                'city_name' => 'Vrgorac',
            ),
            30 => 
            array (
                'id' => 531,
                'city_name' => 'Vrhovine',
            ),
            31 => 
            array (
                'id' => 532,
                'city_name' => 'Vrlika',
            ),
            32 => 
            array (
                'id' => 533,
                'city_name' => 'Vrpolje',
            ),
            33 => 
            array (
                'id' => 534,
                'city_name' => 'Vrsar-Orsera',
            ),
            34 => 
            array (
                'id' => 535,
                'city_name' => 'Vrsi',
            ),
            35 => 
            array (
                'id' => 536,
                'city_name' => 'Vuka',
            ),
            36 => 
            array (
                'id' => 537,
                'city_name' => 'Vukovar',
            ),
            37 => 
            array (
                'id' => 538,
                'city_name' => 'Zabok',
            ),
            38 => 
            array (
                'id' => 539,
                'city_name' => 'Zadar',
            ),
            39 => 
            array (
                'id' => 540,
                'city_name' => 'Zadvarje',
            ),
            40 => 
            array (
                'id' => 541,
                'city_name' => 'Zagorska Sela',
            ),
            41 => 
            array (
                'id' => 542,
                'city_name' => 'Zagreb',
            ),
            42 => 
            array (
                'id' => 543,
                'city_name' => 'Zagvozd',
            ),
            43 => 
            array (
                'id' => 544,
                'city_name' => 'Zaprešić',
            ),
            44 => 
            array (
                'id' => 545,
                'city_name' => 'Zažablje',
            ),
            45 => 
            array (
                'id' => 546,
                'city_name' => 'Zdenci',
            ),
            46 => 
            array (
                'id' => 547,
                'city_name' => 'Zemunik Donji',
            ),
            47 => 
            array (
                'id' => 548,
                'city_name' => 'Zlatar',
            ),
            48 => 
            array (
                'id' => 549,
                'city_name' => 'Zlatar Bistrica',
            ),
            49 => 
            array (
                'id' => 550,
                'city_name' => 'Zmijavci',
            ),
            50 => 
            array (
                'id' => 551,
                'city_name' => 'Zrinski Topolovac',
            ),
            51 => 
            array (
                'id' => 552,
                'city_name' => 'Žakanje',
            ),
            52 => 
            array (
                'id' => 553,
                'city_name' => 'Žminj',
            ),
            53 => 
            array (
                'id' => 554,
                'city_name' => 'Žumberak',
            ),
            54 => 
            array (
                'id' => 555,
                'city_name' => 'Župa dubrovačka',
            ),
            55 => 
            array (
                'id' => 556,
                'city_name' => 'Županja',
            ),
        ));
        
        
    }
}
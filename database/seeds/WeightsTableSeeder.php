<?php

use Illuminate\Database\Seeder;

class WeightsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('weights')->delete();

		DB::table('weights')->insert(
			[
				0 => [ 'value' => '1000' ],
				1 => [ 'value' => '2500' ],
				2 => [ 'value' => '5000' ],
				3 => [ 'value' => '10000' ],
				4 => [ 'value' => '15000' ],
				5 => [ 'value' => '20000' ]
			]
		);
	}
}

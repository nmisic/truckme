<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();

		/**
		 * @TODO: this will change when more profile info will be added.
		 * Test user
		 * pass: nmisic
		 */

        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Nikola',
                'email' => 'nmisic@nmisic.com',
                'password' => '$2y$10$JAo3izS.cYlAEgdFK9Ihwug7dKrqQHsdosUBxjCqIMw2sHuVxsIji',
                'remember_token' => NULL,
                'created_at' => '2017-07-29 15:17:45',
                'updated_at' => '2017-07-29 15:17:45',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}
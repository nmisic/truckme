<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddsTableDateIsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('adds', function (Blueprint $table) {
        $table->dateTime('date')->nullable()->default(NULL)->change();
        $table->dateTime('date_to')->nullable()->default(NULL)->change();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('adds', function (Blueprint $table) {
        $table->dateTime('date')->change();
        $table->dateTime('date_to')->after('date')->change();
      });
    }
}

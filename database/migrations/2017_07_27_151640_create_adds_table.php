<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('adds', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->string('description')->nullable()->default('');
			$table->string('contact')->nullable()->default('');
			$table->integer('start_point')->unsigned()->nullable();
			$table->foreign('start_point')->references('id')->on('cities');
			$table->integer('end_point')->unsigned()->nullable();
			$table->foreign('end_point')->references('id')->on('cities');
			$table->integer('category_id')->unsigned();
			$table->foreign('category_id')->references('id')->on('truck_categories');
			$table->string('weight')->nullable()->default('');
			$table->string('dimensions')->nullable()->default('');
			$table->dateTime('date');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('adds');
	}
}
